﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MLBot
{
    /// <summary>
    /// 文档类型
    /// 先整一些常用的进来，其它的以后再整
    /// </summary>
    [Author("Linyee", "2019-02-01")]
    public class MIME
    {
        public const string json = "application/json";
        public const string xml = "application/xml";
        public const string text = "text/plain";
        public const string htm = "text/html";
        public const string html = "text/html";
        public const string css = "text/css";

        public const string bin = "application/octet-stream";
        public const string apk = "application/vnd.android.package-archive";
    }
}
